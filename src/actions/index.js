import * as actionType from './ActionType';

// get data from people API to authenticate the user
export function getPeopleAction() {  
  return function(dispatch) {
    dispatch({
      type: actionType.GET_PEOPLE_REQUEST
    });
    return fetch('https://swapi.co/api/people/')
        .then(response => response.json().then(body => ({ response, body })))
        .then(({ response, body }) => {
          if (!response.ok) {
            dispatch({
              type: actionType.GET_PEOPLE_FAILURE,
              error: body.error
            });
          } else {
            dispatch({
              type: actionType.GET_PEOPLE_SUCCESS,
              payload: body
            });
        }
      })    
  }
}

// get data from planet API
export function getPlanetAction() {  
  return function(dispatch) {
    dispatch({
      type: actionType.GET_PLANET_REQUEST
    });
    return fetch('https://swapi.co/api/planets/')
        .then(response => response.json().then(body => ({ response, body })))
        .then(({ response, body }) => {
          if (!response.ok) {
            dispatch({
              type: actionType.GET_PLANET_FAILURE,
              error: body.error
            });
          } else {
            dispatch({
              type: actionType.GET_PLANET_SUCCESS,
              payload: body
            });
        }
      })    
  }
}