import * as actionType from '../actions/ActionType';

const INITIAL_STATE = {
    isFetching: false,
    error: undefined,
    data: null
};
  
//people reducer to return new updated state
function getPeopleReducer(state = INITIAL_STATE, action) {
switch (action.type) {
    case actionType.GET_PEOPLE_REQUEST:
    return Object.assign({}, state, {
        isFetching: true
    });
    case actionType.GET_PEOPLE_SUCCESS:   
    return Object.assign({}, state, {
        isFetching: true,
        data: action.payload
    });
    case actionType.GET_PEOPLE_FAILURE:
    return Object.assign({}, state, {
        isFetching: false,
        error: action.error
    });
    default:
    return state;
}
}

export default getPeopleReducer


