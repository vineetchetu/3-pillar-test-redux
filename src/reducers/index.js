import { combineReducers } from 'redux';
import getPeopleReducer from './getPeopleReducer';
import getPlanetReducer from './getPlanetReducer';

const rootReducer = combineReducers({
    getPeopleReducer,
    getPlanetReducer
})

export default rootReducer
