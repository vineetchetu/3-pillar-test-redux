import * as actionType from '../actions/ActionType';

const INITIAL_STATE = {
    isFetching: false,
    error: undefined,
    data: null
};
 
//planet reducer to return new updated state
function getPlanetReducer(state = INITIAL_STATE, action) {
switch (action.type) {
    case actionType.GET_PLANET_REQUEST:
    return Object.assign({}, state, {
        isFetching: true
    });
    case actionType.GET_PLANET_SUCCESS:   
    return Object.assign({}, state, {
        isFetching: true,
        data: action.payload
    });
    case actionType.GET_PLANET_FAILURE:
    return Object.assign({}, state, {
        isFetching: false,
        error: action.error
    });
    default:
    return state;
}
}

export default getPlanetReducer


