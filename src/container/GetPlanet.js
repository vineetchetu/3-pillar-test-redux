import React, { Component } from 'react';
import { connect } from "react-redux";
import Planet from '../components/Planet'
import SearchBar from '../components/SearchBar'
import { getPlanetAction } from '../actions'

class GetPlanet extends Component{
    constructor(props){
        super(props)
        this.state={
            data:null
        }
    }

    // get data from planet API
    componentWillMount(){
        this.props.getPlanetAction();
    }

    // call logout from parent
    user_logout = () => {
        this.props.logout()
    }

    //Search data in planet table
    filter = (query) => {	
        const regex = new RegExp(query, 'i');
        const prevdata = this.props.planetdata.data.results
		const filteredItems = prevdata.filter((item) => {
			return regex.test(item.name);
        })
        var filterResults = {results:filteredItems}
		this.setState({
            data: filterResults
        })
	}
   
    render(){
        let planetResult = null
        if(this.state.data === null){
            if(this.props.planetdata.data !== null){
                planetResult = this.props.planetdata.data.results
            }        
        } else {
            planetResult = this.state.data.results
        }        
        return(
            <div className='container'>                
                <SearchBar onFilter={this.filter} />         
                <input type='button' name='logout' value='Logout'
                onClick={this.user_logout} style={{'marginLeft':'100px'}}/>
                <legend>Planet Data</legend>
                <Planet planetData={planetResult} />
            </div>
        )}
}
const mapStateToProps = state => ({
    planetdata: state.getPlanetReducer
  });
  
  const mapDispatchToProps = (dispatch) => {
    return {
      getPlanetAction: () => dispatch(getPlanetAction())
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(GetPlanet);
//export default GetPlanet;