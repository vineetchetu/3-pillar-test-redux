import React from 'react';
import { Table } from 'reactstrap';

const Planet = (props) => {    
    return(<Table striped>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Rotational Period</th>
            <th>Orbital Period</th>
            <th>Diameter</th>
            <th>Climate</th>
            <th>Gravity</th>
            <th>Terrain</th>
            <th>Surface Water</th>
            <th>Population</th>
          </tr>
        </thead>
        <tbody>
            {props.planetData === null ? '' :
            props.planetData.map((item, index) =>
            <tr key={index}>
                <th scope="row">{index}</th>
                <td>{item.name}</td> 
                <td>{item.rotation_period}</td> 
                <td>{item.orbital_period}</td> 
                <td>{item.diameter}</td> 
                <td>{item.climate}</td> 
                <td>{item.gravity}</td> 
                <td>{item.terrain}</td> 
                <td>{item.surface_water}</td> 
                <td>{item.population}</td>                                   
            </tr>
            )}                   
        </tbody>
      </Table>
    )
}
export default Planet;