import React, { Component } from 'react';

class SearchBar extends Component{
	constructor(){
		super()
			this.state = {           
				query:'',			
			};
	}

  handleChange = (event) => {
    this.setState({query: event.target.value}, () => {
			this.props.onFilter(this.state.query)
		});
  }
    
  render() {
	//   {this.state.query}
	return (
		<input type="search" placeholder="Serach By name" onChange={this.handleChange} value={this.state.query} />
	)
  }
}

export default SearchBar;