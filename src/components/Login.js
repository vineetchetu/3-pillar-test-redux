import React, { Component } from 'react';
import { connect } from "react-redux";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import GetPlanet from '../container/GetPlanet'
import { getPeopleAction } from '../actions'

class Login extends Component{
    constructor(){
        super()
        this.state={
            username:'',
            password:'',
            userData:'',
            userLoggedIn:false,       
        }
    }

    // get data from people API
    componentDidMount(){      
        this.props.getPeopleAction();
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value,
        })
    }

    onSubmit = () => {
        let username = this.state.username
        let password = this.state.password
        let userData = this.props.userDataDetails.data.results
        let birthYearData = userData.filter((data) => 
          data.birth_year === password && data.name === username
        )
        if(birthYearData.length > 0){
          this.setState({
            userLoggedIn:true
          })
          sessionStorage.userLoggedIn = true;
        }
    }

    // logout user
    logout = () => {
      this.setState({
        userLoggedIn:false
      })
      sessionStorage.userLoggedIn = false;
    }

    render(){      
        if((this.state.userLoggedIn == 'true') || (sessionStorage.userLoggedIn == 'true')){
          return(
            <GetPlanet logout={this.logout} />
          )
        } else {
        return(
            <div className='container'>                
                <Form>
                  <FormGroup>
                    <Label fclass="control-label col-sm-2">Email</Label>
                    <Input type="text" 
                      value={this.state.username} 
                      name="username" 
                      placeholder="Username" 
                      onChange={this.onChange} />
                  </FormGroup>
                  <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input type="password" 
                      value={this.state.password} 
                      name="password" 
                      placeholder="Passwordr" 
                      onChange={this.onChange} />
                  </FormGroup>       
                  <Button onClick={this.onSubmit}>Submit</Button>
                </Form>
            </div>
        )}
      }
}

const mapStateToProps = state => ({
  userDataDetails: state.getPeopleReducer
});

const mapDispatchToProps = (dispatch) => {
  return {
    getPeopleAction: () => dispatch(getPeopleAction())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
// export default Login;